// Copyright (C) 2018  INRIA
//
// Treerecs is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Treerecs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TREERECS_EXAMPLES_PATHS_H
#define TREERECS_EXAMPLES_PATHS_H

#include <string>

#include <treerecs/tools/IO/IO.h>

using namespace treerecs;

const std::string test_data_path(TEST_DATA_PATH);

// Polytomies
std::string polytomy_examples_path = test_data_path
                                       + IO::preferred_separator + "polytomy"
                                     + IO::preferred_separator;
std::string published_polytomies_examples_path = polytomy_examples_path
                                                 + IO::preferred_separator
                                                 + "publishedExample"
                                                 + IO::preferred_separator;

// Multipolytomies examples
std::string multipolytomy_examples_path =
    test_data_path + IO::preferred_separator +
    "multipolytomy" + IO::preferred_separator;

// Ensembl compara
std::string ensembl_examples_path =
    test_data_path + IO::preferred_separator +
    "ensembl_compara_73" + IO::preferred_separator;

// Alignments
std::string alignment_examples_path =
    test_data_path + IO::preferred_separator +
    "alignment" + IO::preferred_separator;

// PLL fake examples
std::string pll_fake_examples_path =
    test_data_path + IO::preferred_separator +
    "libpll_examples" + IO::preferred_separator +
    "fake_example" + IO::preferred_separator;
std::string pll_real_examples_path =
    test_data_path + IO::preferred_separator +
    "libpll_examples" + IO::preferred_separator +
    "real_example" + IO::preferred_separator;

// Bipartition list
std::string bipartition_list_path =
    test_data_path + IO::preferred_separator +
    "bipartition_list" + IO::preferred_separator;

// Numbered tree (versatile structure-oriented tree)
std::string numbered_tree_path =
    test_data_path + IO::preferred_separator +
    "numbered_tree" + IO::preferred_separator;

// PhyloXML
std::string phyloxml_path =
    test_data_path + IO::preferred_separator +
    "phyloxml" + IO::preferred_separator;

// Files for testing
std::string test_examples_path =
    test_data_path + IO::preferred_separator +
    "tests" + IO::preferred_separator;

#endif //TREERECS_EXAMPLES_PATHS_H
