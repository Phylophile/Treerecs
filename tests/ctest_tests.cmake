# --------------------
# CTest tests

set(TREERECS_VERSION 1.2)
set(treerecs_bin_regex "/?([a-zA-Z0-9_.-]+/)*treerecs")
set(treerecs_header_regex "Treerecs \\([0-9]\\.[0-9]\\), Inria - Beagle\n")
set(lambda_gene_tree_file
  ${CMAKE_SOURCE_DIR}/tests/data/toy_trees/gt_abc_single_polytomy_newick)
set(usage_string           "Usage:   treerecs -h | --help\n")
string(APPEND usage_string "   or:   treerecs -V | --version\n")
string(APPEND usage_string "   or:   treerecs --usage\n")
string(APPEND usage_string "   or:   treerecs -g GENETREE_FILE -s SPECIESTREE_FILE\n")
string(APPEND usage_string "                  [-S MAP_FILE] [-t BRANCH_SUPPORT_THRESHOLD] [...]\n")
string(APPEND usage_string "   or:   treerecs -x RECPHYLOXML_FILE\n")
string(APPEND usage_string "   or:   treerecs -g GENETREE_FILE --info\n")

# Check exit code for a few well-formed/malformed treerecs commands
# no argument (malformed)
add_test(NAME test_malformed_no_arg
  COMMAND treerecs)
set_property(TEST test_malformed_no_arg
  PROPERTY WILL_FAIL true)
# -g alone (malformed)
add_test(NAME test_malformed_g_alone
  COMMAND treerecs -g ${lambda_gene_tree_file})
set_property(TEST test_malformed_g_alone
  PROPERTY WILL_FAIL true)
# -v and -Y (malformed)
add_test(NAME test_malformed_vY
  COMMAND treerecs -vY)
set_property(TEST test_malformed_vY
  PROPERTY WILL_FAIL true)
# --usage (well-formed)
add_test(NAME test_wellformed_usage
  COMMAND treerecs --usage)
# -g without -s but with --info (well-formed)
add_test(NAME test_wellformed_g_info
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/rm_dir_and_run.sh
  "out"
  "${CMAKE_BINARY_DIR}/bin/treerecs --info
   -g ${CMAKE_SOURCE_DIR}/tests/data/multipolytomy/gene_tree.nhx")
add_test(NAME test_fail_g_info_no_branch_support
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/rm_dir_and_run.sh
  "out"
  "${CMAKE_BINARY_DIR}/bin/treerecs -g ${lambda_gene_tree_file} --info")
set_property(TEST test_fail_g_info_no_branch_support
  PROPERTY WILL_FAIL true)

function(regex_escape out in)
  set(tmp ${in})
  string(REPLACE "^" "\\^" tmp ${tmp})
  string(REPLACE "$" "\\$" tmp ${tmp})
  string(REPLACE "." "\\." tmp ${tmp})
  string(REPLACE "[" "\\[" tmp ${tmp})
  string(REPLACE "]" "\\]" tmp ${tmp})
  string(REPLACE "*" "\\*" tmp ${tmp})
  string(REPLACE "+" "\\+" tmp ${tmp})
  string(REPLACE "?" "\\?" tmp ${tmp})
  string(REPLACE "|" "\\|" tmp ${tmp})
  string(REPLACE "(" "\\(" tmp ${tmp})
  string(REPLACE ")" "\\)" tmp ${tmp})
  set(${out} ${tmp} PARENT_SCOPE)
endfunction()

function(regex_append regexp str)
  string(APPEND ${regexp} ${str})
  set(${regexp} ${${regexp}} PARENT_SCOPE)
endfunction()

function(regex_append_escaped regexp str)
  regex_escape(tmp ${str})
  regex_append(${regexp} ${tmp})
  set(${regexp} ${${regexp}} PARENT_SCOPE)
endfunction()

# Check stdout output of treerecs -V
add_test(NAME test_version
  COMMAND treerecs -V)
set(regex "^")
regex_append(regex ${treerecs_header_regex})
regex_append_escaped(regex "Treerecs ${TREERECS_VERSION}\n")
regex_append(regex "$")
set_property(TEST test_version
  PROPERTY PASS_REGULAR_EXPRESSION "${regex}")

# Check stdout output of treerecs -vY
add_test(NAME test_vY_output
  COMMAND treerecs -vY)
set(regex "^")
regex_append(regex ${treerecs_bin_regex})
regex_append_escaped(regex ": please use a single verbosity option\n")
regex_append_escaped(regex ${usage_string})
regex_append(regex "$")
set_property(TEST test_vY_output
  PROPERTY PASS_REGULAR_EXPRESSION "${regex}")

# Check stdout output of treerecs with no argument -> print usage
add_test(NAME test_usage_output
  COMMAND treerecs --usage)
set(regex "^")
regex_append(regex ${treerecs_header_regex})
regex_append_escaped(regex ${usage_string})
regex_append(regex "$")
set_property(TEST test_usage_output
  PROPERTY PASS_REGULAR_EXPRESSION "${regex}")

# Check output files for a simple treerecs run
set(gt_abc_single_polytomy_newick
  ${CMAKE_SOURCE_DIR}/tests/data/toy_trees/gt_abc_single_polytomy_newick)
set(st_abc_newick
  ${CMAKE_SOURCE_DIR}/tests/data/toy_trees/st_abc_newick)
## Create output directory
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/output/simple)
add_test(NAME test_simple
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/run_test_and_diff_output.sh
  "${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_abc_single_polytomy_newick}
      -s ${st_abc_newick}
      --seed 123456
      -o output/simple
      -O newick:nhx:phyloxml:recphyloxml:svg"
  output/simple
  ${CMAKE_CURRENT_SOURCE_DIR}/data/ref/simple)

# Check that the output dir can be created by treerecs
add_test(NAME test_treerecs_created_outdir
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/rm_dir_and_run.sh
  output/treerecs_created_outdir
  "${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_abc_single_polytomy_newick}
      -s ${st_abc_newick}
      --seed 123456
      -o output/treerecs_created_outdir
      -O svg")

# Check that treerecs refuses to write in a non-empty dir
add_test(NAME test_nonempty_outdir_fail
  COMMAND ${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_abc_single_polytomy_newick}
      -s ${st_abc_newick}
      -o output/simple
      -O svg)
set_property(TEST test_nonempty_outdir_fail
  PROPERTY WILL_FAIL true)

# Check that treerecs does write in a non-empty output dir when --force is set
add_test(NAME test_force_nonempty_outdir
  COMMAND ${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_abc_single_polytomy_newick}
      -s ${st_abc_newick}
      -o output/treerecs_created_outdir
      --force
      -O nhx)

# Check reconciliation with a given threhold value
set(gt_2537_phyml
  ${CMAKE_SOURCE_DIR}/tests/data/tree_2537/phyml_tree)
set(st_2537
  ${CMAKE_SOURCE_DIR}/tests/data/tree_2537/species_tree.txt)
set(map_2537
  ${CMAKE_SOURCE_DIR}/tests/data/tree_2537/tree_2537.smap)
add_test(NAME test_threshold
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/run_test_and_diff_output.sh
  "${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_2537_phyml}
      -s ${st_2537}
      -S ${map_2537}
      -t 0.984
      --seed 987456
      -o output/threshold"
  output/threshold
  ${CMAKE_CURRENT_SOURCE_DIR}/data/ref/threshold)

# Check reconciliation with a given threhold value
set(gt_2537_phyml_simplified
  ${CMAKE_SOURCE_DIR}/tests/data/tree_2537/simplified_phyml_tree)
add_test(NAME test_quantiles_thresholds
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/run_test_and_diff_output.sh
  "${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_2537_phyml_simplified}
      -s ${st_2537}
      -S ${map_2537}
      -t quantiles4
      --seed 682459
      -o output/quantiles_threshold"
  output/quantiles_threshold
  ${CMAKE_CURRENT_SOURCE_DIR}/data/ref/quantiles_threshold)

# Check reconciliation with PLL
set(gt_pll
  ${CMAKE_SOURCE_DIR}/tests/data/pll/gene_tree.txt)
set(st_pll
  ${CMAKE_SOURCE_DIR}/tests/data/pll/species_tree.txt)
set(align_pll
  ${CMAKE_SOURCE_DIR}/tests/data/pll/alignments.txt)
add_test(NAME test_pll
  COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/utils/run_test_and_diff_output.sh
  "${CMAKE_BINARY_DIR}/bin/treerecs
      -g ${gt_pll}
      -s ${st_pll}
      -a ${align_pll}
      -t 0.5
      --seed 987123
      -o output/pll"
  output/pll
  ${CMAKE_CURRENT_SOURCE_DIR}/data/ref/pll)

# Check stdout malformed alignment file
set(align_pll_malformed
  ${CMAKE_SOURCE_DIR}/tests/data/pll/alignments_nomodel.txt)
add_test(NAME test_malformed_align_output
  COMMAND treerecs
    -g ${gt_pll}
    -s ${st_pll}
    -a ${align_pll_malformed}
    -t 0.5
    --seed 987123
    -o output/pll_malformed)
set(regex "")
regex_append(regex ${treerecs_bin_regex})
regex_append_escaped(regex
  ": unknown model msa1.txt, please check your alignment file\n")
regex_append(regex "$")
set_property(TEST test_malformed_align_output
  PROPERTY PASS_REGULAR_EXPRESSION "${regex}")
